package graph;

import java.util.ArrayList;

public interface IDag<T>{
	
	/**
	 * 
	 * @param node
	 * @return the adjacent nodes coming into the node
	 */
	public ArrayList<T> inNeighbours(T node);
	/**
	 * 
	 * @param node
	 * @return the adjacent nodes going out from the node
	 */
	public ArrayList<T> outNeighbours(T node);
	/**
	 * 
	 * @param node
	 * @return the amount of nodes going in to the node
	 */
	public int inDegree(T node);
	/**
	 * 
	 * @param node
	 * @return the amount of nodes going out of the node
	 */
	public int outDegree(T node);
}
