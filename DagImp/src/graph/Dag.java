/**
 * 
 */
package graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author Magnus
 * @param <T> = Node
 * @param <V>
 *
 */
public class Dag<T, V> implements IDag<T> {
	private HashSet<T> Nodes;
	private ArrayList<Edge<T>> dagList;
	private HashMap<T, ArrayList<T>> inNeighbours;
	private HashMap<T, ArrayList<T>> outNeighbours;
	
	public Dag() {
		this.Nodes = new HashSet<T>();
		this.dagList = new ArrayList<Edge<T>>();
		this.inNeighbours = new HashMap<T, ArrayList<T>>();
		this.outNeighbours = new HashMap<T, ArrayList<T>>();
	}

	@Override
	public ArrayList<T> inNeighbours(T node) {
		return inNeighbours.get(node);
	}

	@Override
	public ArrayList<T> outNeighbours(T node) {
		return outNeighbours.get(node);
	}

	@Override
	public int inDegree(T node) {
		
		return inNeighbours.get(node).size();
	}

	@Override
	public int outDegree(T node) {
		return outNeighbours.get(node).size();
	}
	/**
	 * adds the edge you want to add to the appropriate lists.
	 * @param edgeAdd
	 */
	public void addEdge(T fromNode, T toNode) {
		addNodes(fromNode, toNode);
		Edge newEdge = new Edge(fromNode, toNode);
		dagList.add(newEdge);
		if(outNeighbours.containsKey(fromNode)) {
			outNeighbours.get(fromNode).add(toNode);
		}
		else {
			ArrayList<T> addNew = new ArrayList<T>();
			addNew.add(toNode);
			outNeighbours.put(fromNode, addNew);
		}
		if(inNeighbours.containsKey(toNode)) {
			inNeighbours.get(toNode).add(fromNode);
		}
		else {
			ArrayList<T> addIn = new ArrayList<T>();
			addIn.add(fromNode);
			inNeighbours.put(toNode, addIn);
		}
	}
	
	/**
	 * adds the edge you want to add to the appropriate lists.
	 * @param edgeAdd
	 */
	public void addEdge(Edge<T> edgeAdd) {
		addNodes(edgeAdd.a,edgeAdd.b);
		dagList.add(edgeAdd);
		if(outNeighbours.containsKey(edgeAdd.a)) {
			outNeighbours.get(edgeAdd.a).add(edgeAdd.b);
		}
		else {
			ArrayList<T> addNew = new ArrayList<T>();
			addNew.add(edgeAdd.b);
			outNeighbours.put(edgeAdd.a, addNew);
		}
		
		if(inNeighbours.containsKey(edgeAdd.b)) {
			inNeighbours.get(edgeAdd.b).add(edgeAdd.a);
		}
		else {
			ArrayList<T> addIn = new ArrayList<T>();
			addIn.add(edgeAdd.a);
			inNeighbours.put(edgeAdd.b, addIn);
		}
	}
		
	
	/**
	 * helper method for addEdge
	 * @param fromNode
	 * @param toNode
	 */
	private void addNodes(T fromNode, T toNode) {
		Nodes.add(toNode);
		Nodes.add(fromNode);
	}
	/**
	 * 
	 * @return list of edges
	 */
	public ArrayList<Edge<T>> getEdges(){
		return dagList;
		
	}
	
	public HashSet<T> getNodes(){
		return this.Nodes;
	}
	/**
	 * 
	 * @param node
	 * @return true if is source false otherwise.
	 */
	public Boolean isSource(T node) {
		if(inNeighbours.get(node) == null) {
			return true;
		}
		return false;
	}
	/**
	 * 
	 * @param node
	 * @return true if sink false otherwise
	 */
	public Boolean isSink (T node) {
		if(outNeighbours.get(node) == null){
			return true;
		}
		return false;
	}

}
